from flask import Flask
leon = Flask(__name__)

@leon.route("/")
def home():
	return "home"

@leon.route("/users")
def users():
	return "users"

@leon.route("/user")
def user():
	return "user"

@leon.route("/nodes")
def nodes():
	return "nodes"

@leon.route("/node")
def node():
	return "node"

if __name__ == "__main__":
	leon.run(host='0.0.0.0', port=52020)
